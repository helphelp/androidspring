package com.mycompany.android.Entity;

import java.util.ArrayList;
import java.util.List;
import lombok.Data;

/**
 *
 * @author wucy
 */
@Data
public class Pot_Inside {
    private String nickname;
    private String head_picture;
    private String time;
    private String id;
    
    public List<String> taglist ;
    public List<String> picturelist ;
}
