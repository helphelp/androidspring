package com.mycompany.android.Entity;

import java.util.ArrayList;
import java.util.List;
import lombok.Data;

/**
 *
 * @author asus
 */
@Data
public class Personal_Movement_all {
    private String nickname;
    private String time;
    private String head_picture;
    
    private List<String> tagname = new ArrayList<>();
    private List<String> mypicture = new ArrayList<>();
}
