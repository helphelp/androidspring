package com.mycompany.android.Entity;

import lombok.Data;

/**
 *
 * @author asus
 */
@Data
public class Hot_Pot {

    private String tagname;
    private String tagpicture;
}
