package com.mycompany.android.Entity;

import lombok.Data;

/**
 *
 * @author wucy
 */
@Data
public class Pot_Inside_Tag {
    private String id;
    private String tagname;
}
