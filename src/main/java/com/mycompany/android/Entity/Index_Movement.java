package com.mycompany.android.Entity;

import java.util.List;
import lombok.Data;

/**
 *
 * @author wucy
 */
@Data
public class Index_Movement {
    private String nickname;
    private String time;
    private String head_picture;
    private String mypicture;
    private String Account;
    
    private String Movement;
    private String picture_list;
}
