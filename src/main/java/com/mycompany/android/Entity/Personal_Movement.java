package com.mycompany.android.Entity;

import lombok.Data;

/**
 *
 * @author wucy
 */
@Data
public class Personal_Movement {
    private String nickname;
    private String time;
    private String head_picture;
    
    private String mypicture;
    private String tagname;
    private String id;
}
