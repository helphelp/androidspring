package com.mycompany.android.Entity;

import lombok.Data;

/**
 *
 * @author asus
 */


/**
 * Created by Administrator on 2016年11月17日11:23:20.
 */
@Data
public class JsonResult {

    private String code;
    private String message;
    private Object data;
    private int PageCount;
    private int PageNum;


    public JsonResult() {
        this.setCode(ResultCode.SUCCESS);
        this.setMessage("成功！");
    }

    public JsonResult(ResultCode code) {
        this.setCode(code);
        this.setMessage(code.msg());
    }

    public JsonResult(ResultCode code, String message) {
        this.setCode(code);
        this.setMessage(message);
    }
        public JsonResult(ResultCode code, Object data) {
        this.setCode(code);
        this.setMessage(code.msg());
        this.setData(data);
    }

    public JsonResult(ResultCode code, String message, Object data) {

        this.setCode(code);
        this.setMessage(message);
        this.setData(data);
    }

    public void setCode(ResultCode code) {
        this.code = code.val();
    }

    public void setData(Object data) {
        this.data = data;
    }
}
