package com.mycompany.android.Aop;

import com.mycompany.android.Entity.ResultCode;
import com.mycompany.android.Service.SharedService;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 *
 * @author wucy
 */
@Aspect
@Component
public class Login {

    @Pointcut("@annotation(com.mycompany.android.Aop.JudgeLog)")
    public void webLog() {
    }

    @Before("webLog()")
    public ResultCode doBefore(JoinPoint joinPoint) throws Throwable {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        HttpSession session = request.getSession();
        Object args[] = joinPoint.getArgs();
        String username = session.getAttribute("username").toString();
        SharedService sharedService = new SharedService();
        username = sharedService.check_null(username);
        if(username.equals("empty")){
            return ResultCode.NOT_LOGIN;
        }else{
            return ResultCode.SUCCESS_LOGIN;
        }
    }
}
