package com.mycompany.android.Mapper;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Param;

/**
 *
 * @author wucy
 */
public interface LoginMapper {

    //登录
    @Select("SELECT PASSWD FROM Account WHERE username = #{username}")
    String Login_user(@Param("username") String username);

    @Select("SELECT id FROM Account where username=#{username}")
    String my_id(@Param("username") String username);

    //注册
    @Insert("Insert into Account(username,passwd) values (#{username},#{passwd})")
    int Register_user(@Param("username") String username, @Param("passwd") String passwd);

    //初始化用户信息
    @Insert("Insert into User(Account_id,levl,nickname,head_picture) values(#{Account_id},1,'匿名','head.jpg')")
    int Init_User(@Param("Account_id") int Account_id);
    
    //
    @Insert("Insert into Movement(time,Account_id,love_number) values (now(),#{Account_id},0)")
    int init_move(@Param("Account_id") int Account_id);
    
    @Select("Select id From Movement where Account_id = #{Account_id}")
    String get_move(@Param("Account_id") int Account_id);
    
    @Insert("Insert into Picture(Movement_id,mypicture) values (#{Movement_id},'')")
    String insert_pic(@Param("Movement_id") String Account_id);
}
