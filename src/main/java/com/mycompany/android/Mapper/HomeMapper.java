package com.mycompany.android.Mapper;

import com.mycompany.android.Entity.*;
import java.util.List;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

/**
 *
 * @author wucy
 */
public interface HomeMapper {

    //获取username
    @Select("SELECT username FROM Account where id = #{id}")
    String Get_User_username(@Param("id") String id);
    //获取用户id
    @Select("SELECT id FROM Account where username = #{username}")
    String Get_User_id(@Param("username") String username);

    //获取个人信息
    @Select("SELECT nickname,head_picture,levl FROM Personal_Information WHERE username = #{username}")
    Personal_Information Get_Personal_Information(@Param("username") String username);

    //获取关注数
    @Select("SELECT Conner_number FROM Count_num WHERE username = #{username}")
    String Get_Conner_Number(@Param("username") String username);

    //获取粉丝数
    @Select("SELECT fans_number FROM Fans_num WHERE username = #{username}")
    String Get_Fans_Number(@Param("username") String username);

    //获取个人动态
    @Select("SELECT nickname,time,head_picture,id  FROM Personal_movement WHERE username = #{username}")
    List<Personal_Movement> Get_Personal_Movement(@Param("username") String username);
    //获取个人动态图片
    @Select("SELECT Movement_id,tagname,mypicture FROM Personal_movement_Picture where username=#{username}")
    List<Personal_Movement_picture> Get_Personal_Movement_Picture(@Param("username") String username);
    
    @Select("SELECT id FROM my_id where username=#{username}")
    List<String> Get_Movement_id(@Param("username") String username);
    //关注用户
    @Insert("INSERT INTO Conner(Account_id_a,Account_id_b) VALUES(#{account_a},#{account_b})")
    int Conner_user(@Param("account_a") String account_a, @Param("account_b") String account_b);

    //取消关注
    @Delete("Delete from Conner where Account_id_a = #{account_a} and Account_id_b = #{account_b}")
    int Delete_Conner_user(@Param("account_a") String account_a, @Param("account_b") String account_b);
    
    //获取关注列表
    @Select("Select nickname,Account_id_a,head_picture,id from Conner_List where Account_id_a =#{username}")
    List<Conner_User> Get_Conner_List(@Param("username") String username);
    
    //获取粉丝列表
    @Select("Select nickname,Account_id_b,head_picture,id  from Fans_List where Account_id_b =#{username}")
    List<Fans_User> Get_Fans_List(@Param("username") String username);
    
    //删除粉丝
    @Delete("Delete from Conner where Account_id_b = #{account_a} and Account_id_a = #{account_b}")
    int Delete_Fans_user(@Param("account_a") String account_a, @Param("account_b") String account_b);
    
    //修改名字
    @Update("Update User set nickname=#{nickname} where Account_id = #{Account_id}")
    int Update_nickname(@Param("nickname") String nickname,@Param("Account_id") String Account_id);
    
    //发布
    @Insert("Insert into Movement(time,Account_id,love_number) values (now(),#{Account_id},0)")
    int Insert_Movement(@Param("Account_id") String Account_id);
    
    //发布的图片
    @Insert("Insert into Picture(Movement_id,mypicture) values(#{Movement_id},#{picture})")
    int Insert_Picture(@Param("Movement_id") String Movement_id,@Param("picture") String picture);
    
    //获取tag_id
    @Select("SELECT id from TagName where tagname = #{tagname}")
    int Get_TagName(@Param("tagname") String tagname);
    
    //动态的标签
    @Insert("Insert into Tag(Movement_id,TagName_id) values (#{Movement_id},#{Tag_id})")
    int Insert_Tag(@Param("Movement_id") String Movement_id,@Param("Tag_id") String Tag_id);
    
    //修改头像
    @Update("Update User set head_picture=#{head_picture} where Account_id = #{Account_id}")
    int Update_head_picture(@Param("head_picture") String head_picture,@Param("Account_id") String Acount_id);
    
    @Select("Select time From Get_move_id where Account_id = #{Account_id}")
    String get_move(@Param("Account_id") String Account_id);
    
    @Select("select id from Movement where time = #{time}")
    String get_mo_id(@Param("time") String time);
}
