package com.mycompany.android.Mapper;

import com.mycompany.android.Entity.Hot_Pot;
import com.mycompany.android.Entity.Pot_Inside;
import com.mycompany.android.Entity.Pot_Inside_Tag;
import java.util.List;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

/**
 *
 * @author wucy
 */
public interface ImageMapper {

    //获取用户id
    @Select("SELECT id FROM Account where username = #{username}")
    String Get_User_id(@Param("username") String username);

    //获取热门罐子
    @Select("SELECT tagname,tagpicture FROM get_hot_pot")
    List<Hot_Pot> Get_Hot_Pot();

    //获取所有罐子
    @Select("SELECT tagname,tagpicture FROM TagName")
    List<Hot_Pot> Get_All_Pot();

    //获取罐子内容
    @Select("SELECT nickname,head_picture,time,id FROM Inside_pot_one where tagname = #{tagname}")
    List<Pot_Inside> Get_All_Movement(@Param("tagname") String tagname);

    //获取罐子标签
    @Select("SELECT tagname FROM Inside_Pot_tag where id = #{id}")
    List<String> Get_Movement_Tag(@Param("id") String id);

    //获取罐子图片
    @Select("SELECT mypicture FROM Inside_Pot_picture where id = #{id}")
    List<String> Get_Movement_Picture(@Param("id") String id);

    //点赞
    @Insert("Insert into Up(Account_id,Movement_id) values(#{Account_id},#{Movement_id}) ")
    int dian_zan(@Param("Account_id") String Account_id, @Param("Movement_id") String Movement_id);

    @Update("Update Movement set love_number = love_number+1 where Account_id = ?")
    int jia_yi(@Param("Account_id") String Account_id);
}
