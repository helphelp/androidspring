package com.mycompany.android.Mapper;

import com.mycompany.android.Entity.Index_Movement;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 *
 * @author wucy
 */
public interface IndexMapper {
    
    //获取主页信息
    @Select("SELECT nickname,time,head_picture,mypicture,Account,Movement FROM Index_information")
    List<Index_Movement> Get_Index_Information();
    
    //获取动态内容
    @Select("SELECT mypicture from Picture where Movement_id = #{Movement_id}")
    List<String> Get_Data_Picture(@Param("Movement_id") String Movement_id);
}
