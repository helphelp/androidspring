package com.mycompany.android.Controller;

import com.mycompany.android.Entity.JsonResult;
import com.mycompany.android.Service.ImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author wucy
 */
@RestController
public class ImageController {

    @Autowired
    ImageService imageService;

    //获取热门
    @RequestMapping("/Get_Hot_Pot")
    public JsonResult do_Ge_Hot_Pot() {
        return imageService.Get_Hot_Pot();
    }

    //获取所有罐子
    @RequestMapping("/Get_All_Pot")
    public JsonResult do_Get_All_Pot() {
        return imageService.Get_All_Pot();
    }

    //罐子内部按时间排序
    @RequestMapping("/Get_Pot_Movement")
    public JsonResult do_Get_Pot_Movement(@RequestParam("pot_name") String pot_name) {
        return imageService.Get_Pot_Tag(pot_name);
    }

    //点赞
    @RequestMapping("/Give_up")
    public JsonResult do_give_up(@RequestParam("Movement_id") String Movement_id, @RequestParam("username") String username) {
        return imageService.dianzan(username, Movement_id);
    }

    //返回点赞过的列表
//    //按喜欢排序
//    @RequestMapping("/like_sort")
//    public JsonResult do_like_sort(@RequestParam("Pot_inside") Pot_Inside[] pot_inside){
//        
//    }
}
