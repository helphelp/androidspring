package com.mycompany.android.Controller;

import com.mycompany.android.Entity.JsonResult;
import com.mycompany.android.Service.IndexService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author wucy
 */
@RestController
public class IndexController {
    @Autowired
    IndexService indexservice;
            
    @RequestMapping("/main_pager")
    public JsonResult do_main_pager(){
        JsonResult temp = new JsonResult();
        temp = indexservice.do_main_pager();
        return temp;
    }
}
