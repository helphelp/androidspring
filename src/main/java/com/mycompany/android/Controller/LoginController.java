package com.mycompany.android.Controller;

import com.mycompany.android.Entity.JsonResult;
import com.mycompany.android.Entity.ResultCode;
import com.mycompany.android.Service.LoginService;
import java.util.Map;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author wucy
 */
@RestController
public class LoginController {

    @Autowired
    LoginService loginService;

    @RequestMapping("/test")
    public String test() {
        return "hello";
    }
    
    @RequestMapping("/testarray")
    public String[] test(@RequestParam("username") String[] username) {
        
        return username;
    }
    

    //登录
    @RequestMapping("/login")
    public JsonResult do_login(@RequestParam("username") String username, @RequestParam("password") String password, HttpSession session) {
        ResultCode temp = loginService.Login_Service(username, password);
        if (temp.equals(ResultCode.SUCCESS)) {
            System.out.println("ok");
            session.setAttribute("username", username);
//            session.setMaxInactiveInterval(60 * 60 * 24);
        }
        return new JsonResult(temp);
    }

    //登出
    @RequestMapping("/logout")
    public void do_logout(HttpSession session) {
        session.removeAttribute("username");
    }
    
    //注册
    @RequestMapping("/register")
    public JsonResult do_register(@RequestParam("username") String username, @RequestParam("password") String password) {

        ResultCode temp = loginService.Register_Service(username, password);
        return new JsonResult(temp);
    }

    //获取验证码
    @RequestMapping("/identify")
    public JsonResult do_identify(@RequestParam("number") String number) throws Exception {
        Map temp = loginService.phone_ma(number);
        if (temp.get("result").equals(ResultCode.SUCCESS)) {
            //TODO return code
            return new JsonResult(ResultCode.SUCCESS,temp);
        } else {
            return new JsonResult(ResultCode.EXCEPTION);
        }
    }
}
