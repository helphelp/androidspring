package com.mycompany.android.Controller;

import com.mycompany.android.Entity.JsonResult;
import com.mycompany.android.Entity.ResultCode;
import com.mycompany.android.Service.HomeService;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author wucy
 */
@RestController
public class HomeController {

    @Autowired
    HomeService homeService;

    //获取个人信息
    @RequestMapping("/Get_Personal_Information")
    public JsonResult do_Get_Personal_Information(@RequestParam("username") String username) {
        JsonResult temp = homeService.Get_Personal_Information(username);
        return temp;
    }

    @RequestMapping("/Get_Personal_infor")
    public JsonResult do_Get_Personal_Info(@RequestParam("username") String username) {
        JsonResult temp = homeService.Get_Personal_Infor(username);
        return temp;
    }

    //获取个人动态
    @RequestMapping("/Get_Personal_Movement")
    public JsonResult do_Get_Personal_Movement(@RequestParam("username") String username) {
        JsonResult temp = homeService.Get_Personal_Movement(username);
        return temp;
    }

    @RequestMapping("/Get_Personal_Move")
    public JsonResult do_Get_Personal_Move(@RequestParam("username") String username) {
        JsonResult temp = homeService.Get_Personal_Move(username);
        return temp;
    }

    //关注
    @RequestMapping("/Conner")
    public JsonResult do_Conner(@RequestParam("id") String id, @RequestParam("username") String username) {
        return homeService.Conner(username, id);
    }

    //取消关注
    @RequestMapping("/Delete_Conner")
    public JsonResult do_Delete_Conner(@RequestParam("id") String id, @RequestParam("username") String username) {
        return homeService.Delete_Conner(username, id);
    }

    //查看关注列表
    @RequestMapping("/Conner_List")
    public JsonResult do_Conner_List(@RequestParam("username") String username) {
        return homeService.Conner_List(username);
    }

    //查看粉丝列表
    @RequestMapping("/Fans_List")
    public JsonResult do_Fans_List(@RequestParam("username") String username) {
        return homeService.Fans_List(username);
    }

    //移除粉丝
    @RequestMapping("/Delete_fans")
    public JsonResult do_Delete_fans(@RequestParam("id") String id, @RequestParam("username") String username) {
        return homeService.Delete_fans(username, id);
    }

    //发动态
    @RequestMapping("/Move")
    public JsonResult do_Move(@RequestParam("tag") String[] tag, @RequestParam("username") String username, @RequestParam("file") String[] file) throws Exception {
        Arrays.stream(tag).forEach(x -> System.out.println(x));
        return homeService.Move(username, file, tag);
    }

    //修改昵称
    @RequestMapping("/changeNickname")
    public JsonResult do_change_nickname(@RequestParam("username") String username, @RequestParam("nickname") String nickname) {
        return homeService.Change_Nickname(username, nickname);
    }

    //上传头像
    @RequestMapping("/head_picture")
    public JsonResult do_picture(@RequestParam("bitmapStr") String bitmapStr, @RequestParam("username") String username) throws Exception {
        homeService.string2image(bitmapStr, username);
        return new JsonResult(ResultCode.SUCCESS);
    }
    //访客是否被关注
//    @RequestMapping("/isconner")
}
