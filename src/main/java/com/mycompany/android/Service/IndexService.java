package com.mycompany.android.Service;

import com.mycompany.android.Entity.Index_Movement;
import com.mycompany.android.Entity.JsonResult;
import com.mycompany.android.Entity.ResultCode;
import com.mycompany.android.Mapper.IndexMapper;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author wucy
 */
@Service
public class IndexService {

    @Autowired
    IndexMapper indexmapper;

    //获取主页信息
    public JsonResult do_main_pager() {
        List<Index_Movement> temp = new ArrayList();
        temp = indexmapper.Get_Index_Information();
        if (temp.isEmpty()) {
            return new JsonResult(ResultCode.FAIL);
        };
        temp.stream().forEach(x -> {
            String Movement_id = x.getMovement();
            List<String> one_picture = new ArrayList<>();
            one_picture = indexmapper.Get_Data_Picture(Movement_id);
            x.setMypicture(one_picture.get(0));
        });

        return new JsonResult(ResultCode.SUCCESS,temp);
    }
}
