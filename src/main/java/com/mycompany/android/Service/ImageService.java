package com.mycompany.android.Service;

import com.google.gson.Gson;
import com.mycompany.android.Entity.Hot_Pot;
import com.mycompany.android.Entity.JsonResult;
import com.mycompany.android.Entity.Pot_Inside;
import com.mycompany.android.Entity.ResultCode;
import com.mycompany.android.Mapper.ImageMapper;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sun.misc.BASE64Decoder;

/**
 *
 * @author wucy
 */
@Service
public class ImageService {

    @Autowired
    ImageMapper imageMapper;

    private SharedService sharedService = new SharedService();

    //获取热门罐子
    public JsonResult Get_Hot_Pot() {
        List<Hot_Pot> temp = new ArrayList<>();
        temp = imageMapper.Get_Hot_Pot();
        return new JsonResult(ResultCode.SUCCESS, temp);
    }

    //获取所有罐子
    public JsonResult Get_All_Pot() {
        List<Hot_Pot> temp = new ArrayList<>();
        temp = imageMapper.Get_All_Pot();
        return new JsonResult(ResultCode.SUCCESS, temp);
    }

    //获取罐子内容
    public JsonResult Get_Pot_Tag(String tagName) {
        List<Pot_Inside> temp = new ArrayList<>();
        temp = imageMapper.Get_All_Movement(tagName);
        temp.stream().forEach(x -> {
            List<String> tag = new ArrayList<>();
            List<String> picture = new ArrayList<>();
            tag = imageMapper.Get_Movement_Tag(x.getId());
            picture = imageMapper.Get_Movement_Picture(x.getId());
            x.picturelist = new ArrayList<>();
            x.taglist = new ArrayList<>();
            x.setTaglist(tag);
            x.setPicturelist(picture);
        });
        return new JsonResult(ResultCode.SUCCESS, temp);
    }

    //点赞
    public JsonResult dianzan(String username, String Movement_id) {
        String id = imageMapper.Get_User_id(username);
        int temp = imageMapper.dian_zan(id, Movement_id);
        int temp2 = imageMapper.jia_yi(id);
        return new JsonResult(ResultCode.SUCCESS);
    }

    


}
