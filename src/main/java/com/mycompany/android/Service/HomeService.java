package com.mycompany.android.Service;

import com.mycompany.android.Entity.*;
import com.mycompany.android.Mapper.HomeMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.FileOutputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;
import sun.misc.BASE64Decoder;

/**
 *
 * @author wucy
 */
@Service
public class HomeService {

    @Autowired
    HomeMapper homemapper;

    private SharedService sharedService = new SharedService();

    //获取个人信息
    public JsonResult Get_Personal_Information(String username) {
        Map temp = new HashMap<>();
        Personal_Information person = homemapper.Get_Personal_Information(username);
        String conner_number = homemapper.Get_Conner_Number(username);
        String fans_number = homemapper.Get_Fans_Number(username);
        temp.put("Information", person);
        temp.put("conner_number", conner_number);
        temp.put("fans_number", fans_number);

        return new JsonResult(ResultCode.SUCCESS, temp);
    }

    //获取个人信息
    public JsonResult Get_Personal_Infor(String userid) {
        String username = homemapper.Get_User_username(userid);
        Map temp = new HashMap<>();
        Personal_Information person = homemapper.Get_Personal_Information(username);
        String conner_number = homemapper.Get_Conner_Number(username);
        String fans_number = homemapper.Get_Fans_Number(username);
        temp.put("Information", person);
        temp.put("conner_number", conner_number);
        temp.put("fans_number", fans_number);

        return new JsonResult(ResultCode.SUCCESS, temp);
    }

    //获取个人动态
    public JsonResult Get_Personal_Movement(String username) {
        List<Personal_Movement> personal_Movement = new ArrayList<>();
        List<Personal_Movement_picture> personal_Movement_pictures = new ArrayList<>();
        List<String> id = new ArrayList<>();
        List<Personal_Movement_all> templist = new ArrayList<>();
        personal_Movement = homemapper.Get_Personal_Movement(username);
        personal_Movement_pictures = homemapper.Get_Personal_Movement_Picture(username);
        id = homemapper.Get_Movement_id(username);
        for (int i = 0; i < id.size(); i++) {
            String idd = id.get(i);
            List<Personal_Movement> a = personal_Movement.stream().filter(y -> y.getId().equals(idd)).collect(Collectors.toList());
            Personal_Movement_all temp = new Personal_Movement_all();
            temp.setNickname(a.get(0).getNickname());
            temp.setTime(a.get(0).getTime());
            temp.setHead_picture(a.get(0).getHead_picture());
            List<Personal_Movement_picture> b = personal_Movement_pictures.stream().filter(y -> y.getMovement_id().equals(idd)).collect(Collectors.toList());
            List<String> img = new ArrayList<>();
            List<String> tag = new ArrayList<>();
            b.stream().forEach(x -> img.add(x.getMypicture()));
            b.stream().forEach(x -> tag.add(x.getTagname()));
            temp.setMypicture(img);
            temp.setTagname(tag);
            templist.add(temp);
//            System.err.println(templist.size());
        }
        return new JsonResult(ResultCode.SUCCESS, templist);
    }

    public JsonResult Get_Personal_Move(String userid) {
        String username = homemapper.Get_User_username(userid);
        List<Personal_Movement> personal_Movement = new ArrayList<>();
        List<Personal_Movement_picture> personal_Movement_pictures = new ArrayList<>();
        List<String> id = new ArrayList<>();
        List<Personal_Movement_all> templist = new ArrayList<>();
        personal_Movement = homemapper.Get_Personal_Movement(username);
        personal_Movement_pictures = homemapper.Get_Personal_Movement_Picture(username);
        id = homemapper.Get_Movement_id(username);
        for (int i = 0; i < id.size(); i++) {
            String idd = id.get(i);
            List<Personal_Movement> a = personal_Movement.stream().filter(y -> y.getId().equals(idd)).collect(Collectors.toList());
            Personal_Movement_all temp = new Personal_Movement_all();
            temp.setNickname(a.get(0).getNickname());
            temp.setTime(a.get(0).getTime());
            temp.setHead_picture(a.get(0).getHead_picture());
            List<Personal_Movement_picture> b = personal_Movement_pictures.stream().filter(y -> y.getMovement_id().equals(idd)).collect(Collectors.toList());
            List<String> img = new ArrayList<>();
            List<String> tag = new ArrayList<>();
            b.stream().forEach(x -> img.add(x.getMypicture()));
            b.stream().forEach(x -> tag.add(x.getTagname()));
            temp.setMypicture(img);
            temp.setTagname(tag);
            templist.add(temp);
//            System.err.println(templist.size());
        }
        return new JsonResult(ResultCode.SUCCESS, templist);
    }

    //关注用户
    public JsonResult Conner(String username, String conner_name) {
        String id_me = homemapper.Get_User_id(username);
        int temp = homemapper.Conner_user(id_me, conner_name);
        return new JsonResult(ResultCode.SUCCESS);
    }

    //取消关注
    public JsonResult Delete_Conner(String username, String conner_name) {
        String id_me = homemapper.Get_User_id(username);
        int temp = homemapper.Delete_Conner_user(id_me, conner_name);
        return new JsonResult(ResultCode.SUCCESS);
    }

    //查看关注列表
    public JsonResult Conner_List(String username) {
        List<Conner_User> temp = new ArrayList<>();
        String id_me = homemapper.Get_User_id(username);
        temp = homemapper.Get_Conner_List(id_me);
        Optional<List<Conner_User>> optional2 = Optional.ofNullable(temp);
        if (!optional2.isPresent() || temp.isEmpty()) {
            return new JsonResult(ResultCode.NOT_DATA);
        }
        return new JsonResult(ResultCode.SUCCESS, temp);
    }

    //查看粉丝列表
    public JsonResult Fans_List(String username) {
        List<Fans_User> temp = new ArrayList<>();
        String id_me = homemapper.Get_User_id(username);
        temp = homemapper.Get_Fans_List(id_me);
        Optional<List<Fans_User>> optional2 = Optional.ofNullable(temp);
        if (!optional2.isPresent() || temp.isEmpty()) {
            return new JsonResult(ResultCode.NOT_DATA);
        }
        return new JsonResult(ResultCode.SUCCESS, temp);
    }

    //删除粉丝
    public JsonResult Delete_fans(String username, String conner_name) {
        String id_me = homemapper.Get_User_id(username);
        int temp = homemapper.Delete_Conner_user(id_me, conner_name);
        return new JsonResult(ResultCode.SUCCESS);
    }

    //修改昵称
    public JsonResult Change_Nickname(String username, String nickname) {
        String id_me = homemapper.Get_User_id(username);
        int temp = homemapper.Update_nickname(nickname, id_me);
        return new JsonResult(ResultCode.SUCCESS);
    }

    //发表
    public JsonResult Move(String username, String[] file, String[] tag) throws Exception {
        //发布
        String id = homemapper.Get_User_id(username);
        homemapper.Insert_Movement(id);
        String time = homemapper.get_move(id);
        String Movement_id = homemapper.get_mo_id(time);
        // 要上传的目标文件存放路径
        String localPath = "/var/www/html/picture/";
        //String localPath = "";
        SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");//设置日期格式

        List<String> mypicture = new ArrayList<>();
        mypicture = Arrays.asList(file);
//        System.out.println(mypicture.size());
        for (int i = 0; i < mypicture.size(); i++) {

            String name = localPath + df.format(new Date()) + String.valueOf(i + 1) + ".jpg";
            do_decode(mypicture.get(i), name);
           String a = df.format(new Date()) + String.valueOf(i + 1) + ".jpg";
            homemapper.Insert_Picture(String.valueOf(Movement_id), a);
        }

        //上传标签
        List<String> temp = new ArrayList<>();
        temp = Arrays.asList(tag);
        temp.stream().forEach(x -> {
//            System.out.println(x);
            int i = homemapper.Get_TagName(x);
            homemapper.Insert_Tag(String.valueOf(Movement_id), String.valueOf(i));
        });
        return new JsonResult(ResultCode.SUCCESS);
    }

    //上传头像
    public boolean string2image(String imgStr, String username) throws Exception {
        String id_me = homemapper.Get_User_id(username);
        SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
        String name = df.format(new Date());
        String localpath = "/var/www/html/picture";
        String imgFile = localpath + name + ".jpg";
        String file = name + ".jpg";
        do_decode(imgStr, imgFile);
        homemapper.Update_head_picture(imgFile, file);
        return true;
    }

    public boolean do_decode(String imgStr, String imgFile) {
        imgStr = imgStr.replace(" ", "+");
        imgStr = imgStr.replace("\r\n", "");
        if (imgStr == null) {
            return false;
        }
        BASE64Decoder decoder = new BASE64Decoder();
        try {
            // Base64解码
            byte[] b = decoder.decodeBuffer(imgStr);
            for (int i = 0; i < b.length; ++i) {
                if (b[i] < 0) {// 调整异常数据
                    b[i] += 256;
                }
            }

            OutputStream out = new FileOutputStream(imgFile);
            out.write(b);
            out.flush();
            out.close();
            return true;
        } catch (Exception e) {
            System.out.println(e);
            return false;
        }
    }

}
