package com.mycompany.android.Service;

import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsRequest;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsResponse;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;
import com.aliyuncs.profile.IClientProfile;
import com.mycompany.android.Entity.ResultCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.HashMap;
import java.util.Map;
import com.mycompany.android.Mapper.LoginMapper;

/**
 *
 * @author wucy
 */
@Service
public class LoginService {

    @Autowired
    LoginMapper mapper;

    private SharedService sharedService = new SharedService();

    //登陆
    public ResultCode Login_Service(String username, String password) {
        String passwd = mapper.Login_user(username);
        passwd = sharedService.check_null(passwd);
        if (passwd.equals("empty")) {
            return ResultCode.NOT_DATA;
        } else if (passwd.equals(password)) {
            return ResultCode.SUCCESS;
        } else {
            return ResultCode.PARAMS_ERROR;
        }
    }
    
    //判断是否登录
    public ResultCode Judge_Service(String username) {
        username = sharedService.check_null(username);
        if (username.equals("empty")) {
            return ResultCode.NOT_LOGIN;
        } else {
            return ResultCode.SUCCESS;
        }
    }

    //注册
    public ResultCode Register_Service(String username, String password) {
        int temp = mapper.Register_user(username, password);
//        System.out.println(temp);
        int tt = Integer.valueOf(mapper.my_id(username));
        mapper.Init_User(tt);
//        mapper.init_move(tt);
//        String a = mapper.get_move(tt);
//        mapper.insert_pic(a);
        return ResultCode.SUCCESS;
    }

    //获取验证码
    public Map<String, Object> phone_ma(String phone) throws Exception {
        Map temp = new HashMap();
        //设置超时时间-可自行调整
        System.setProperty("sun.net.client.defaultConnectTimeout", "30000");
        System.setProperty("sun.net.client.defaultReadTimeout", "10000");
        //初始化ascClient需要的几个参数
        final String product = "Dysmsapi";//短信API产品名称（短信产品名固定，无需修改）
        final String domain = "dysmsapi.aliyuncs.com";//短信API产品域名（接口地址固定，无需修改）
        //替换成你的AK
        final String accessKeyId = "LTAIuhDuTKodBTRc";//你的accessKeyId,参考本文档步骤2
        final String accessKeySecret = "qby8GYWznpgybs90O0Jt1Fr1OSzXVr";//你的accessKeySecret，参考本文档步骤2
        //初始化ascClient,暂时不支持多region（请勿修改）
        IClientProfile profile = DefaultProfile.getProfile("cn-hangzhou", accessKeyId,
                accessKeySecret);
        DefaultProfile.addEndpoint("cn-hangzhou", "cn-hangzhou", product, domain);
        IAcsClient acsClient = new DefaultAcsClient(profile);
        //组装请求对象
        SendSmsRequest request = new SendSmsRequest();
        //使用post提交
        request.setMethod(MethodType.POST);
        //必填:待发送手机号。支持以逗号分隔的形式进行批量调用，批量上限为1000个手机号码,批量调用相对于单条调用及时性稍有延迟,验证码类型的短信推荐使用单条调用的方式；发送国际/港澳台消息时，接收号码格式为国际区号+号码，如“85200000000”
        request.setPhoneNumbers(phone);
        //必填:短信签名-可在短信控制台中找到
        request.setSignName("oj");
        //必填:短信模板-可在短信控制台中找到，发送国际/港澳台消息时，请使用国际/港澳台短信模版
        request.setTemplateCode("SMS_150577022");
        //可选:模板中的变量替换JSON串,如模板内容为"亲爱的${name},您的验证码为${code}"时,此处的值为
        //友情提示:如果JSON中需要带换行符,请参照标准的JSON协议对换行符的要求,比如短信内容中包含\r\n的情况在JSON中需要表示成\\r\\n,否则会导致JSON在服务端解析失败
        String randomnumber = sharedService.get_randomnumber();
        request.setTemplateParam("{\"name\":\"Tom\", \"code\":" + randomnumber + "}");
        //可选-上行短信扩展码(扩展码字段控制在7位或以下，无特殊需求用户请忽略此字段)
        //request.setSmsUpExtendCode("90997");
        //可选:outId为提供给业务方扩展字段,最终在短信回执消息中将此值带回给调用者
        request.setOutId("yourOutId");
//请求失败这里会抛ClientException异常
        SendSmsResponse sendSmsResponse = acsClient.getAcsResponse(request);
//        if (sendSmsResponse.getCode() != null && sendSmsResponse.getCode().equals("OK")) {
            //请求成功
            temp.put("result", ResultCode.SUCCESS);
            temp.put("number", randomnumber);
            return temp;
//        }
//        temp.put("result", ResultCode.EXCEPTION);
//        return temp;
    }
}
