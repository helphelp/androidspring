DROP VIEW if EXISTS Index_information;
DROP VIEW if EXISTS Personal_Information;
DROP VIEW if EXISTS Count_num;
DROP VIEW if EXISTS Fans_num;
DROP VIEW if EXISTS Personal_movement;
DROP VIEW if EXISTS Conner_List;
DROP VIEW if EXISTS Fans_List;
DROP VIEW if EXISTS get_hot_pot;
DROP VIEW if EXISTS Personal_movement_Picture;
DROP VIEW if EXISTS my_id;
DROP VIEW if EXISTS Inside_pot_one;
DROP VIEW if EXISTS Inside_Pot_tag;
DROP VIEW if EXISTS Inside_Pot_picture;
DROP VIEW if EXISTS Get_move_id;
#获取主页信息
CREATE view Index_information as
select nickname,time,head_picture,mypicture,Account.id Account,Movement.id Movement
from Movement,`User`,Picture,Account
where to_days(time) = to_days(now())
AND Picture.Movement_id = Movement.id
AND Movement.Account_id = `User`.Account_id
AND Account.id = Movement.Account_id
ORDER BY love_number DESC
LIMIT 4;

#获取个人页面
CREATE VIEW Personal_Information as
SELECT nickname,head_picture,levl,username
from `User`,Account
WHERE `User`.Account_id = Account.id;

#获取关注数
CREATE VIEW Count_num as
SELECT Count(*) Conner_number,any_value(username) username
FROM Conner,Account
where Account.id = Conner.Account_id_a
GROUP BY Account_id_a;

#获取粉丝数
CREATE VIEW Fans_num as
SELECT Count(*) fans_number,any_value(username) username
FROM Conner,Account
where Account.id = Conner.Account_id_b
GROUP BY Account_id_b;

#获取个人动态
CREATE view Personal_movement as
select nickname,time,head_picture,mypicture,tagname,username,Movement.id
from Movement,`User`,Picture,Tag,TagName,Account
where to_days(time) = to_days(now())
AND Picture.Movement_id = Movement.id
AND Movement.Account_id = `User`.Account_id
AND Tag.Movement_id = Movement.id
AND Tag.TagName_id = TagName.id
AND Account.id = Movement.Account_id
ORDER  BY nickname,time DESC;

#获取个人发的动态id
CREATE view my_id as
select Movement.id,username
from Movement,Account
where Account_id = Account.id;

#获取个人动态的图片和Tag
CREATE view Personal_movement_Picture as
select Tag.Movement_id,tagname,mypicture,username
from TagName,Tag,Picture,Account,Movement
where Tag.Movement_id = Picture.Movement_id
and Tag.TagName_id = TagName.id
and Movement.id = Tag.Movement_id
and Account_id = Account.id
order by Movement_id;

#获取关注列表
CREATE view Conner_List as
SELECT  DISTINCT nickname,Account_id_a,head_picture,Account.id
from Conner,`User`,Account
WHERE Account_id_b = `User`.Account_id
and  User.Account_id = Account.id
ORDER BY Account_id_a 
;

#获取粉丝列表
CREATE view Fans_List as
SELECT  DISTINCT nickname,Account_id_b,head_picture,Account.id
from Conner,`User`,Account
WHERE Account_id_a = `User`.Account_id
and User.Account_id = Account.id
ORDER BY Account_id_b 
;

#获取热门罐子
CREATE view get_hot_pot as
SELECT tagname,count(TagName_id) num,tagpicture
from Tag,TagName
WHERE Tag.TagName_id = TagName.id
GROUP BY TagName_id
ORDER BY num DESC
limit 3;

Create view Inside_pot_one as
select nickname,head_picture,time,Movement.id,tagname
from User,Movement,Tag,TagName
where User.Account_id = Movement.Account_id
And Tag.Movement_id = Movement.id
and TagName_id = TagName.id
order by tagname;

Create view Inside_Pot_tag as
select tagname,Movement.id
from TagName,Movement,Tag
where TagName.id = Tag.TagName_id
and Tag.Movement_id = Movement.id
order by Movement.id;

Create view Inside_Pot_picture as
select mypicture,Movement.id
from Movement,Picture
where Picture.Movement_id = Movement.id
order by Movement.id;

create VIEW Get_move_id as
Select Account_id,max(time) time , any_value(id) id
From Movement 
group by Account_id;