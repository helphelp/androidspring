drop database Image ;
CREATE DATABASE Image DEFAULT character set 'utf8mb4';
use Image;
CREATE TABLE Account(
		id int unsigned auto_increment,
		username varchar(250) unique not null comment '手机号',
		passwd varchar(225) not null comment '密码',
		
		PRIMARY KEY (id)
)comment='账户';

CREATE TABLE User(
		id int unsigned auto_increment,
		Account_id int unsigned not null,
		levl int UNSIGNED not null COMMENT '用户等级',
		nickname VARCHAR(225) not null comment '呢称',
		head_picture VARCHAR(225) comment '头像',
	
	  FOREIGN key (Account_id) references Account (id) on delete cascade on update cascade,
	  PRIMARY KEY (id)
)comment='用户表';

CREATE TABLE Conner(
		id int UNSIGNED auto_increment,
		Account_id_a int unsigned not null comment '我',
		Account_id_b int UNSIGNED not null comment '我关注的人',
		
		
		FOREIGN key (Account_id_a) references Account (id) on delete cascade on update cascade,
		FOREIGN key (Account_id_b) references Account (id) on delete cascade on update cascade,
		PRIMARY KEY (id)
)comment='关注列表';

CREATE TABLE Movement(
		id int UNSIGNED auto_increment,
		time TIMESTAMP not null comment '创建时间',
		Account_id int UNSIGNED not null,
		love_number int UNSIGNED not null comment '喜欢数',

		FOREIGN key (Account_id) references Account (id) on delete cascade on update cascade,
		PRIMARY KEY (id)
)comment='动态';

CREATE TABLE TagName(
		id int UNSIGNED auto_increment,
		tagname varchar(250) comment '标签名字',
		tagpicture varchar(250) comment '标签图片',
		
		PRIMARY KEY (id)
)comment='标签名字';

CREATE TABLE Tag(
		id int UNSIGNED auto_increment,
		Movement_id int UNSIGNED not null,
		TagName_id int UNSIGNED not null ,
			
		FOREIGN key (Movement_id) references Movement (id) on delete cascade on update cascade,	
		FOREIGN key (TagName_id) references TagName (id) on delete cascade on update cascade,		
		PRIMARY KEY (id)
)COMMENT='动态标签';

CREATE TABLE Picture(
		id int UNSIGNED auto_increment,
		Movement_id int UNSIGNED not null,
		mypicture VARCHAR(250) COMMENT '图片路径',
		
		FOREIGN key (Movement_id) references Movement (id) on delete cascade on update cascade,		
		PRIMARY KEY (id)
)COMMENT='动态内容';

CREATE TABLE Up(
	id int UNSIGNED auto_increment,
	Account_id int unsigned not null,
	Movement_id int UNSIGNED not null,
	
	FOREIGN key (Movement_id) references Movement (id) on delete cascade on update cascade,	
	FOREIGN key (Account_id) references Account (id) on delete cascade on update cascade,
	PRIMARY KEY (id)
)COMMENT='点赞';